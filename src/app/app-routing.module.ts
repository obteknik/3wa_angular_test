import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ContactsComponent} from './contacts/contacts.component';
import {RouterModule, Routes} from '@angular/router';
import {ContactDetailComponent} from './contact-detail/contact-detail.component';
import {ContactFormComponent} from './contact-form/contact-form.component';
import {ContactEditComponent} from './contact-edit/contact-edit.component';


/*Définition de la constante pour les routes*/
const routes: Routes = [
    { path: '', redirectTo: '/contacts', pathMatch: 'full' },
    { path: 'contacts', component: ContactsComponent },
    { path: 'contact/view/:id', component: ContactDetailComponent },
    { path: 'contact/add', component: ContactFormComponent },
    { path: 'contact/edit/:id', component: ContactEditComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)], /*Chargement des routes dans l'application*/
    exports: [RouterModule]
})

export class AppRoutingModule { }
