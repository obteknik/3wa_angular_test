import { Component, OnInit } from '@angular/core';
import {Contact} from '../contact';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {ContactService} from '../contact.service';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.scss']
})
export class ContactEditComponent implements OnInit {

    contact: Contact;
    updateFormContact: FormGroup;
    id: string;

    constructor(
        private route: ActivatedRoute,
        private fb: FormBuilder,
        private router: Router,
        private cS: ContactService
    ) { }

  ngOnInit() {

      this.id = this.route.snapshot.paramMap.get('id'); // id dans l'url

      this.initUpdateForm(); // initialisation du formulaire

      // mise à jour du formulaire après l'instanciation de ce dernier
      this.cS.getContact(this.id).subscribe(contact => {
              // on récupère l'instance du formulaire et on met à jour les champs du formulaire
              // avec la méthode patchValue du formGroup
              this.updateFormContact.patchValue(contact);
          }
      );
  }

    initUpdateForm() {

        this.updateFormContact = this.fb.group(
            {
                firstname: new FormControl('', [
                    Validators.required,
                    Validators.minLength(4)
                ]),
                lastname: new FormControl('', [
                    Validators.required,
                    Validators.minLength(3)
                ]),
                phoneNumber: new FormControl(),
                address: new FormControl('', [
                    Validators.required,
                    Validators.min(10)
                ]),
                birthDate: new FormControl()
            }
        );

    }

    // getter pour la validation dans le formulaire errors
    get firstname() { return this.updateFormContact.get('firstname'); }
    get lastname() { return this.updateFormContact.get('lastname'); }
    get address() { return this.updateFormContact.get('address'); }

    onSubmit() {
        let contact: Contact;
        contact = this.updateFormContact.value;
        contact.id = this.id;

        /**
         * @todo observer methods next and error
         */
        this.cS.updateContact(contact.id, contact).subscribe(
            () => {
                this.router.navigate(['/contacts'], { queryParams: { message: 'success updated resource' } });
            }
        )
    }

}