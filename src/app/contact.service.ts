import { Injectable } from '@angular/core';
import {Contact} from './contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

    subjectContact = new Subject<Contact>();

  constructor() { }

    getContact(id: string): Observable<Contact> {

        return this.get<Contact>(this.albumsUrl + `/${id}/.json`).pipe(
            map(contact => contact)
        );
    }

    updateContact(ref: string, contact: Contact): Observable<void> {
        console.log(ref);
        return this.http.put<void>(this.albumsUrl + `/${ref}/.json`, album);
    }

}
