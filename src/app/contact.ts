export class Contact {
    id: number;
    firstname: string;
    lastname: string;
    phoneNumber: string;
    address: string;
    birthDate: string;
}
