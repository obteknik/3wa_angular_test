import { Contact } from './contact';

export const CONTACTS: Contact[] = [
    {
        'id': 1,
        'firstname': 'Olivier',
        'lastname': 'BRASSART',
        'phoneNumber': '0623124578',
        'address': '12 rue du Pavé 45000 Orléans',
        'birthDate': '1973-02-20'
    },
    {
        'id': 2,
        'firstname': 'Paul',
        'lastname': 'CHARVET',
        'phoneNumber': '0789457845',
        'address': '142 bd Foch 75009 Paris',
        'birthDate': '1982-08-13'
    },
    {
        'id': 3,
        'firstname': 'Delphine',
        'lastname': 'ANDRET',
        'phoneNumber': '0756897400',
        'address': '54 rue du puits 78000 Versailles',
        'birthDate': '1978-12-25'
    }
];
