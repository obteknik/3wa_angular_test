import { Component, OnInit } from '@angular/core';
import { Contact } from '../contact';
import {CONTACTS} from '../mock-contacts';


@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent implements OnInit {

    titlePage: string = "Contacts";
    contacts: Contact[] = CONTACTS;
    selectedContact: Contact;

    constructor() { }

  ngOnInit() {
  }

    onSelect(contact: Contact) {
        console.log(contact);
        this.selectedContact = contact;
    }

}
