import { Component, OnInit } from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import { FormBuilder, FormGroup } from '@angular/forms';
import {Contact} from '../contact';
import {CONTACTS} from '../mock-contacts';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.scss']
})
export class ContactFormComponent implements OnInit {

    contacts: Contact[] = CONTACTS;
    reactiveForm: FormGroup;
    submittedContact: Contact;


  constructor(private fb: FormBuilder, private route: ActivatedRoute,
              private router: Router) {

      this.reactiveForm = this.fb.group({

          firstname: new FormControl('', [Validators.required, Validators.minLength(4)]),
          lastname: new FormControl('', [Validators.required, Validators.minLength(3)]),
          phoneNumber: new FormControl(),
          address: new FormControl('', [Validators.required, Validators.minLength(10)]),
          birthDate: new FormControl()
      });
  }

    ngOnInit() {
    }

    onSubmit() {
        console.log('reactiveForm', this.reactiveForm.value);

        this.submittedContact = {
            'id': Math.floor(Math.random() * Math.floor(100)),
            'firstname': this.reactiveForm.value['firstname'],
            'lastname': this.reactiveForm.value['lastname'],
            'phoneNumber': this.reactiveForm.value['phoneNumber'],
            'address': this.reactiveForm.value['address'],
            'birthDate': this.reactiveForm.value['birthDate']
        };

        this.contacts.push(this.submittedContact);

        // console.log(this.submittedContact);
        // console.log(this.contacts);

        // Redirection
        this.router.navigate(['/contacts']);
    }

}
